#include "onevoter.h"
/****************************************************************
 * 'onevoter.cc'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 12-01-2016--10:20:07
 *
**/

static const string kTag = "ONEVOTER: ";

/****************************************************************
* Constructor.
**/
OneVoter::OneVoter() {
}
/****************************************************************
 * Constructor. 
 * @param: sequence, arrival in seconds, and durration
 * these attributes are unique to each voter, 
 * placement (or sequence) in the queue the time they arrive
 * and the durration they are in line 
**/
OneVoter::OneVoter(int sequence, int arrival_seconds,
int duration_seconds) {
  sequence_ = sequence;
  time_arrival_seconds_ = arrival_seconds;
  time_start_voting_seconds_ = 0;
  time_vote_duration_seconds_ = duration_seconds;
  which_station_ = -1;
}//end Constructor OneVoter

/****************************************************************
 * Deconstructor.
**/
OneVoter::~OneVoter() {
}

/****************************************************************
* Accessors and mutators.
**/
/****************************************************************
 * Accessor 'GetTimeArrival'
 * 
 * Returns:
 *   the time this voter arrived in seconds (not min)
**/
int OneVoter::GetTimeArrival() const {
  return time_arrival_seconds_;
}
/****************************************************************
 * Accessor 'GetTimeWaiting'
 * 
 * Returns:
 *   the time this voter was in line in seconds (not min)
**/
int OneVoter::GetTimeWaiting() const {
  return time_waiting_seconds_;
}

/****************************************************************
 * Accessor 'GetStationNumber'
 * 
 * Returns:
 *   the station this voter used to cast their ballot
**/
int OneVoter::GetStationNumber() const {
  return which_station_;
}
/****************************************************************
 * Accessor 'GetTimeDoneVoting'
 * 
 * Returns:
 *  the time it took for this voter to vote in sec
**/
int OneVoter::GetTimeDoneVoting() const {
  return time_start_voting_seconds_ + time_vote_duration_seconds_;
}
/****************************************************************
 * Accessor 'GetTimeInQ'
 * 
 * Returns:
 *  the time this voter was in queue to vote
**/
int OneVoter::GetTimeInQ() const {
  return time_start_voting_seconds_ - time_arrival_seconds_;
}

/****************************************************************
* General functions.
**/

/****************************************************************
 * Function 'AssignStation'
 * @param: polling station number and the start time in sec
 *
 * this function will assign a polling station to a voter
 * it will then compute the time it took for this voter
 * to complete the time it took for this voter to 
 * start the voting process based on arrival time and time
 * spent in the queue. 
**/
void OneVoter::AssignStation(int station_number,
int start_time_seconds) {
  //overwrite the variables with real values from dummy value
  //assign station
  which_station_ = station_number;
  //start time for this voter
  time_start_voting_seconds_ = start_time_seconds;
  //time it takes to vote plus the time it takes to vote
  time_done_voting_seconds_ = time_start_voting_seconds_
  + time_vote_duration_seconds_;
  //time in line is difference in time this voter arrived and 
  // the time to start voting 
  time_waiting_seconds_ = time_start_voting_seconds_
  - time_arrival_seconds_;
}//end AssignStation

/****************************************************************
 * Function 'GetTOD'
 * @param time in seconds
 * this function computes the time of day based on the time given
 * in seconds, function is not used in code
**/
string OneVoter::GetTOD(int time_in_seconds) const {
  //  int offset_hours = 6;
  int offset_hours = 0;
  string s = "";
  return this->ConvertTime(time_in_seconds + offset_hours*3600);
}//end GetTOD

/****************************************************************
 * Function 'ConvertTime'
 * This function will take in a given time in seconds and 
 * convert that time to a more readable format 
 * HH:MM:SS by dividing the seconds by the largest unit each 
 * time and then using the Utils to format the output as a
 * string to print in a different function
**/
string OneVoter::ConvertTime(int time_in_seconds) const {
  int hours = 0;
  int minutes = 0;
  int seconds = 0;
  string s = "";

  hours = time_in_seconds / 3600;
  minutes = (time_in_seconds - 3600*hours) / 60;
  seconds = (time_in_seconds - 3600*hours - 60*minutes);

  s += Utils::Format(time_in_seconds, 6); 
  //hours
  if(hours < 0)
    s += " 00";
  else if(hours < 10)
    s += " 0" + Utils::Format(hours, 1);
  else
   s += " " + Utils::Format(hours, 2);
  //minutes
  if(minutes < 0)
    s += ":00";
  else if(minutes < 10) 
    s += ":0" + Utils::Format(minutes, 1);
  else
    s += ":" + Utils::Format(minutes, 2);
  //seconds
  if(seconds < 0)
    s += ":00";
  else if(seconds < 10)
    s += ":0" + Utils::Format(seconds, 1);
  else
    s += ":" + Utils::Format(seconds, 2);

  return s;
} // string OneVoter::ConvertTime(int time_in_seconds) const

/****************************************************************
 * Function 'ToString'
 * this function returns a formatted string for the arrival 
 * times of voters in HH:MM:SS and the station the voter
 * is at and the durration of the voting process, to be used 
 * int the output file 
**/
string OneVoter::ToString() {
  string s = kTag;

  s += Utils::Format(sequence_, 7);
  s += ": ";
  s += Utils::Format(this->GetTOD(time_arrival_seconds_));
  s += " ";
  s += Utils::Format(this->GetTOD(time_start_voting_seconds_));
  s += " ";
  s += Utils::Format(this->ConvertTime(time_vote_duration_seconds_));
  s += " ";
  s += Utils::Format(this->GetTOD(time_start_voting_seconds_ 
    + time_vote_duration_seconds_));
  s += " ";
  s += Utils::Format(this->ConvertTime(GetTimeInQ()));
  s += ": ";
  s += Utils::Format(which_station_, 4);

return s;
} // string OneVoter::toString()

/****************************************************************
 * Function 'ToStringHeadder'
 * this function format a headder for the section to print to the
 * output file
**/
string OneVoter::ToStringHeader() {
  string s = kTag;
  s += "    Seq        Arr           Start               Dur             End            Wait         Stn";
  return s;
}





/****************************************************************
 * End 'onevoter.cc'
**/
