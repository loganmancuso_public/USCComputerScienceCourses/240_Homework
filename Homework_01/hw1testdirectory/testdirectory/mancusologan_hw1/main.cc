#include <iostream>
using namespace std;

/****************************************************************
 * Main program for Homework 1.
 *
 * Author/copyright: Mancuso Logan. All rights reserved.
 * Date: 14 May 2016
**/

int main(int argc, char *argv[]) {
  cout << "Hello, world." << endl;
  cout << "My name is Mancuso Logan." << endl;

  return 0;
}
