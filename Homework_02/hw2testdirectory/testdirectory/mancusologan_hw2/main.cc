#include "main.h"


/*******************************************************************************
 * 45678901234567890123456789012345678901234567890123456789012345678901234567890
 * Main program for Homework 2.
 *
 * Author/copyright:  Duncan Buell. All rights reserved.
 * Used with permission and modified by: Logan Mancuso
 * Date: 25 August 2016
**/

int ComputePayoffInMonths(double amount, double monthly_payment, double yearly_rate);

/*******************************************************************************
**/
int main( ) {

  //variables
  double amount = 0.00; //starting
  double yearly_rate = 0.00;
  double monthly_payment = 125.00; //fist months payment
  int month = 0;
  cout << "Loan Calculator:" << endl;
  //read file from stream
  cin >> amount;
  cin >> yearly_rate;
  //read file stream
  cout << "reading from file $" << amount << " at " << yearly_rate << "%" << endl;
  if ( amount <= 0.00 || yearly_rate <= 0.00) {
    cout << " input value too small " << endl;
    return 0;
  }
  //change to decimal and compute
  while ( month > 180 || month == 0 ) {
    month = ComputePayoffInMonths( amount, monthly_payment, yearly_rate );
    if ( month > 180 ) {
      cout << "Loan will take " << month << " months to pay off, recalculating"
      << " better payment timeline" << endl;
      monthly_payment += 25.00;
    } else {
      cout << "It will take " << month << " months at $" << monthly_payment
      << " to pay off $" << amount <<" optimal payment timeline found"<< endl;
      break;
    }
  }

  cout << "Loan Computation Complete" << endl;

  return 0;
} // int main( )


/*******************************************************************************
**/
int ComputePayoffInMonths(double amount, double monthly_payment, double yearly_rate) {
  int month = 0;
  double monthly_rate = ( yearly_rate / ( 100.00 * 12.00 ));
  cout << "\n\tMonth\tRate\t\tPayment\t\tPrinciple\tInterest\tRemaining" << endl;
  double amount_remaining = amount;
  while ( amount_remaining > 0.0 ) {
    double interest_incurred = ( amount_remaining * monthly_rate );
    double payment_on_principle = ( monthly_payment - interest_incurred );
    amount_remaining = ( amount_remaining - payment_on_principle );
    //print method from main.h
    cout << "\t" << to_string(month) << "\t" << to_string(yearly_rate) << "%\t$"
    << to_string(monthly_payment) << "\t$" << to_string(payment_on_principle) <<
    "\t$" << to_string(interest_incurred) << "\t$" << (amount_remaining) <<  endl;
    ++month;
  }
  return month;
} // int ComputePayoffInMonths(double amount, double monthly_payment,



