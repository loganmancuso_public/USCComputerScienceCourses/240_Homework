#include "main.h"

/****************************************************************
 * Main program for Homework 6.
 *
 * Author/copyright:  Duncan Buell. All rights reserved.
 * Date: 4 November 2014
 * Edited By Mancuso Logan
 * Last Edit Date: 10/24/2016 14:31:17
 *
**/


static const string kTag = "Main: ";

int main(int argc, char *argv[])
{
  string in_filename = "";
  string out_filename = "";
  string log_filename = "";
  ofstream out_stream;

  Scanner scanner;
  PacketAssembler assembler;

  Utils::CheckArgs(3, argc, argv, "infilename outfilename logfilename");
  in_filename = static_cast<string>(argv[1]);
  out_filename = static_cast<string>(argv[2]);
  log_filename = static_cast<string>(argv[3]);

  Utils::LogFileOpen(log_filename);
  Utils::FileOpen(out_stream, out_filename);

  Utils::log_stream << kTag << "Beginning execution\n";
  Utils::log_stream.flush();

  Utils::log_stream << kTag << "infile  '" << in_filename << "'\n";
  Utils::log_stream << kTag << "outfile '" << out_filename << "'\n";
  Utils::log_stream << kTag << "logfile '" << log_filename << "'\n";

  scanner.OpenFile(in_filename);

  assembler.RunForever(scanner, out_stream);

  Utils::log_stream << kTag << "Ending execution\n";
  Utils::log_stream.flush();

  Utils::FileClose(out_stream);
  Utils::FileClose(Utils::log_stream);

  return 0;
}
