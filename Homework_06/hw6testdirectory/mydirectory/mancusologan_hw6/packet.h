/****************************************************************
 * Header file for a single packet.
 *
 * Author/copyright:  Duncan Buell
 * Date: 8 August 2016
 * Edited By Mancuso Logan
 * Last Edit Date: 10/24/2016 14:31:17
 *
**/

#ifndef PACKET_H
#define PACKET_H

#include <iostream>
using namespace std;

#include "../../Utilities/scanner.h"
#include "../../Utilities/scanline.h"


class Packet {
public:
 Packet();
 Packet(Scanner& scanner);
 virtual ~Packet();

 //getters
 int GetMessageID() const;
 int GetPacketID() const;
 int GetPacketCount() const;
 string GetPayload() const;

 //functions
 string ToString() const;
 bool Equals(const Packet& p) const;
 //method will be passed a vector from which to parse
 void ReadPacket(vector<string>& input);

 static const int kDummyMessageID_ = -3333;
 static const int kDummyPacketID_ = -222;
 static const int kDummyPacketCount_ = -11;
 const string kDummyPayload_ = " ";

private:

 int messageID_ = kDummyMessageID_;
 int packet_count_ = kDummyPacketCount_;
 int packetID_ = kDummyPacketID_;
 string payload_ = kDummyPayload_;

};

#endif
