/****************************************************************
 * Rather generic header file that includes the 'Utilities' code.
 *
 * Author/copyright:  Duncan Buell
 * Date: 8 August 2016
 * Edited By Mancuso Logan
 * Last Edit Date: 10/24/2016 14:31:17
 *
**/
#ifndef MAIN_H
#define MAIN_H

#include <iostream>
using namespace std;

#include "../../Utilities/utils.h"
#include "../../Utilities/scanner.h"
#include "../../Utilities/scanline.h"

#include "packetassembler.h"

#endif // MAIN_H
