/****************************************************************
 * Header file for the phone book record for homework 3.
 *
 * Author/copyright:  Duncan Buell
 * Used with permission and modified by: Mancuso Logan
 * Date last modified: 10 September 2016 
 *
**/

#ifndef RECORD_H
#define RECORD_H

#include <iostream>
#include <string>
#include <stack>
using namespace std;

#include "../../Utilities/scanner.h"
#include "../../Utilities/scanline.h"

class Record
{
public:
 Record();
 virtual ~Record();

//getters and setters

//surname
 string GetSurname() const;

//phone number
 string GetPhoneNumber() const;

//other methods
 bool CompareName(Record that);
 bool CompareNumber(Record that);
 Record ReadData(const string& input);
 string ToString();

private:
 //variables
 string forename_;
 string other_name_;
 string surname_;
 string phone_number_;
};

#endif
